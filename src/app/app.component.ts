import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { UsersService } from './services/users.service';
import { FilesService } from './services/files.service';
import { TokenService } from './services/token.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  imgParent = '';
  showImage = true;
  token = '';
  imgRta = '';

  constructor(
    private authService: AuthService,
    private usersService: UsersService,
    private flService: FilesService,
    private tokenService: TokenService
    ){}

  onLoaded(img: string) {
    // console.log('Log del padre', img);
  }

  ngOnInit(): void {
    const token = this.tokenService.getToken();
    if (token) {
      this.authService.getProfile()
        .subscribe();
    }
  }

  toggleImg() {
    this.showImage = !this.showImage;
  }

  createUser() {
    this.usersService.create({
      name: 'Sebas',
      email: 'sebas@dev.com',
      password: '123456',
      role: 'customer'
    })
    .subscribe(rta => {
      console.log(rta);
    });
  }

  downloadFile() {
    this.flService.getFile('my.pdf', 'https://young-sands-07814.herokuapp.com/api/files/dummy.pdf', 'application/pdf')
      .subscribe()
  }

  onUpload(event: Event) {
    const element = event.target as HTMLInputElement;
    const file = element.files?.item(0);

    if (file) {
      this.flService.uploadFile(file)
        .subscribe(res => {
          this.imgRta = res.location;
        });
    }

  }

}
