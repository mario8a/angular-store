import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


import { switchMap } from 'rxjs/operators';
import { ProductsService } from '../../../services/products.service';
import { Product } from '../../../models/product.model';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  categoryId: string | null = null;
  limit = 10;
  offset = 0;
  products: Product[] = [];
  productId: string | null = null;

  constructor(private route: ActivatedRoute, private productsService: ProductsService) { }

  // ngOnInit(): void {
  //   this.route.paramMap.subscribe(params => {
  //     this.categoryId =  params.get('id');
  //     if (this.categoryId) {
  //       this.productsService.getByCategory(this.categoryId, this.limit.toString(), this.offset.toString())
  //         .subscribe(data => {
  //           this.products = data;
  //         })
  //     }
  //   });
  // }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap(params => {
        this.categoryId = params.get('id');
        if (this.categoryId) {
          return this.productsService.getByCategory(this.categoryId, this.limit.toString(), this.offset.toString())
        }
        return [];
      })
    )
    .subscribe((data) => {
      this.products = data;
    });

    this.route.queryParamMap.subscribe(params => {
      this.productId = params.get('product');
      // console.log(this.productId);
    });
  }

}
