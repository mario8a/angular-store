import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from '../../../services/products.service';
import { Product } from '../../../models/product.model';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  products: Product[] = [];
  limit = 10;
  offset = 0;
  productId: string | null = null;

  constructor(private productsService: ProductsService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    // Este servicio consulta a una API por lo que es asincrono el servicio
    // this.productsService.getAllProducts()
    //   .subscribe(data => {
    //     this.products = data;
    //   });
    //getProductsByPage
    this.productsService.getAllProducts('10', '0')
      .subscribe(data => {
        this.products = data;
        this.offset += this.limit;
        // console.log(data);
    });
    this.route.queryParamMap.subscribe(params => {
      this.productId = params.get('product');
      console.log(this.productId);
    });
  }

  onLoadMore() {
    this.productsService.getProductsByPage(this.limit.toString(), this.offset.toString())
      .subscribe(data => {
        this.products = this.products.concat(data);
        this.offset += this.limit;
      });
  }

}
