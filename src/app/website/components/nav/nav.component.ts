import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../../services/store.service';
import { AuthService } from '../../../services/auth.service';
import { User } from '../../../models/user.model';
import { switchMap, map } from 'rxjs/operators';
import { CategoriesService } from '../../../services/categories.service';
import { Category } from '../../../models/category.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  activeMenu = false;
  counter = 0;
  profile: User | null = null;
  categories: Category[] = [];

  constructor(
    private storeService: StoreService,
    private authService: AuthService,
    private categoriesService: CategoriesService,
    private route: Router) { }

  ngOnInit(): void {
    this.storeService.myCart$.subscribe(products => {
      this.counter = products.length;
    });
    this.getAllCategories();
    this.authService.user$
    .subscribe(data => {
      this.profile = data;
    });
  }

  toggleMenu() {
    this.activeMenu = !this.activeMenu;
  }

  login() {
    // this.authService.loginAndGet('admin@mail.com', 'admin123')
    this.authService.loginAndGet('john@mail.com', 'changene')
    .subscribe((user) => {
      // this.profile = user;
      // Aqui pasa algo raro :/
      // No debe estar user
      this.route.navigate(['/profile']);
    });
    // this.authService.login('sebas@dev.com', '123456')
    //   .pipe(
    //     switchMap((token) => {
    //       return this.authService.profile(token.access_token);
    //     })
    //   )
    //   .subscribe(user => {
    //     this.profile = user;
    //   });
  }

  // getProfile() {
  //   this.authService.profile()
  //     .subscribe(user => {
  //       this.profile = user;
  //     });
  // }

  getAllCategories() {
    this.categoriesService.getAll()
      .subscribe(data => {
        this.categories = data;
      });
  }

  logout() {
    this.authService.logout();
    this.profile = null;
    this.route.navigate(['/home']);
  }

}
