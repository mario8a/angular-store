import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { StoreService } from '../../../services/store.service';
import { ProductsService } from '../../../services/products.service';
import { switchMap } from 'rxjs/operators';
import { zip } from 'rxjs';
import { Product, CreateProductDTO, UpdateProductDTO } from '../../../models/product.model';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent {

  @Input() products: Product[] = [];
  // @Input() productId: string | null = null;
  @Input() set productId(id: string | null) {
    if (id) {
      this.onShowDetail(id);
    }
  }
  @Output() loadMore = new EventEmitter();

  myShopingCart: Product[] = [];
  total = 0;
  today = new Date();
  date = new Date(2021, 1, 21);
  showProductDetail = false;
  producChosen: Product = {
    id: '',
    price: 0,
    images: [],
    title: '',
    description: '',
    category: {
      id: '',
      name: ''
    }
  };

  statusDetail: 'loading' | 'success' | 'error' | 'init' = 'init';

  constructor(
    private storeService: StoreService,
    private productsService: ProductsService
  ) {
    // Como esta funcion no es asincrona se puede colocar en el constructor
    this.myShopingCart = this.storeService.getShopingCart();
  }

  onAddToShoppingCart(product: Product) {
    this.storeService.addProduct(product);
    this.total = this.storeService.getTotal();
  }

  toggleProductDetail() {
    this.showProductDetail = !this.showProductDetail;
  }

  onShowDetail(id: string) {
    this.statusDetail = 'loading';
    // console.log('IdProduct', id);
    // this.toggleProductDetail();
    if (!this.showProductDetail) {
      this.showProductDetail = true;
    }
    this.productsService.getProduct(id)
      .subscribe(data => {
        // console.log('product: ', data);
        this.producChosen = data;
        this.statusDetail = 'success';
      }, ErrorMsg => {
        console.log(ErrorMsg);
        window.alert(ErrorMsg)
        this.statusDetail = 'error';
      });
  }

  // CallbackHell
  // RECOMENDABLE PONER EN EL SERVICIO
  readAndUpdate(id: string) {
    this.productsService.getProduct(id)
    .pipe(
      switchMap((product) => {
        return this.productsService.update(product.id, {title: 'change'});
      })
    )
    .subscribe(data => {
      // console.log(data)
    });
    zip(
    this.productsService.getProduct(id),
    this.productsService.update(id, {title: 'jeje'})
    )
    .subscribe(res => {
      // recibimos la respuesta como un array
      const read = res[0];
      const update = res[1];

    });
  }

  createNewProduct() {
    const product: CreateProductDTO = {
      title: 'Nuevo product',
      description: 'lorem im pasdasdasdas',
      images: ['https://placeimg.com/640/480/any'],
      price: 1000,
      categoryId: 2,
    }
    this.productsService.create(product)
      .subscribe(data => {
        // console.log('Craeted', data);
        this.products.unshift(data);
      });
  }

  updateProduct() {
    const changes: UpdateProductDTO = {
      title: 'Nuevo tiyulo'
    };
    const id = this.producChosen.id;
    // console.log('id chosen', id);
    this.productsService.update(id, changes)
      .subscribe(data => {
        // console.log('UPDATE', data);
        const productIndex = this.products.findIndex(item => item.id === this.producChosen.id);
        this.products[productIndex] = data;
        this.producChosen = data;
      });
  }

  deleteProduct() {
    const id = this.producChosen.id;
    this.productsService.delete(id)
      .subscribe(() => {
        const productIndex = this.products.findIndex(item => item.id === this.producChosen.id);
        this.products.splice(productIndex, 1);
        this.showProductDetail = false;
      });
  }

  onloadMore() {
    this.loadMore.emit();
  }

}
