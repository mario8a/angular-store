import { Component, OnInit, Input, Output, EventEmitter, OnChanges, AfterViewInit, OnDestroy, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-img',
  templateUrl: './img.component.html',
  styleUrls: ['./img.component.scss']
})
export class ImgComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  
  @Input() img: string = '';
  @Output() loaded = new EventEmitter<string>();
  imageDefault = 'https://www.m2crowd.com/core/i/placeholder.png';
  // counter = 0;
  // counterFun: number | undefined;

  constructor() {
    // Before render - Antes de que se monte el componente
    // No correr cosas asincronas (asyng) por ejemplo una peticion
    // SOlo corre una vez
    // Podemos ejeurar cambios de valor inmediatos
    // console.log('constructor', 'Image value--->', this.img);
  }

  ngOnChanges(changes: SimpleChanges) {
    // Before y durante render - Antes de que se monte el componente se ejecuta
    // Su objetivo es estar actualizando los cambios en los inputs
    // Se ejecuta las veces que se actualizemos los inputs de nuestros componentes
    // console.log('ngOnChanges', 'Image value--->', this.img);
    //Esto recibira todos los cambios, TODOS
    // console.log(changes)
  }

  ngOnInit(): void {
    // Before render - Antes de que se monte el componente
    // Aqui si podemos correr cosas asincronas (async - fetch)
    // Solo corre una vez cuando se inicializa el componente
    // console.log('ngOnInit', 'Image value--->', this.img);
    // this.counterFun = window.setInterval(() => {
    //   this.counter += 1;
    //   console.log('Run counter');
    // }, 1000)
  }

  ngAfterViewInit() {
    // after - corre despues del render
    // Normalmente aqui manejamos los componentes hijos
    // console.log('ngAfterViewInit');
  }

  ngOnDestroy(): void {
    //Corre cuando se elimina el componente cuando dejamos de verlo en la interfaz 
    // console.log('ngOnDestroy');
    // window.clearInterval(this.counterFun);
  }

  imgError() {
    this.img = this.imageDefault;
  }

  imageLoaded() {
    // console.log('Load hijo');
    this.loaded.emit(this.img);
  }

}
