import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  private myShopingCart: Product[] = [];
  private myCart = new BehaviorSubject<Product[]>([]); // Debe llevar un estado inicial

  myCart$ = this.myCart.asObservable(); // subscritor

  total = 0;

  constructor() { }

  addProduct(product: Product) {
    this.myShopingCart.push(product);
    this.myCart.next(this.myShopingCart); // Transmitiendo el array de productos
    // SOlo los que etan suscritos al servicio van a recibir la informacion
  }

  getShopingCart() {
    return this.myShopingCart;
  }

  getTotal() {
    return this.total = this.myShopingCart.reduce((sum, item) => sum + item.price, 0);
  }
}
