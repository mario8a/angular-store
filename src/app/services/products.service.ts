import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { retry, catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

import { Product, CreateProductDTO, UpdateProductDTO } from '../models/product.model';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  // "https://young-sands-07814.herokuapp.com",
  private apiUrl: string = `${environment.API_URL}/api`;
  // private apiUrl: string = `https://young-sands-07814.herokuapp.com/api/products/`;


  constructor(private http: HttpClient) { }

  getByCategory(cateId: string, limit?: string, offset?: string) {
    let params = new HttpParams();
    if (limit && offset) {
      params = params.set('limit', limit);
      params = params.set('offset', offset);

    }
    return this.http.get<Product[]>(`${this.apiUrl}/categories/${cateId}/products`, {params});
  }

  getAllProducts(limit?: string, offset?: string) {
    let params = new HttpParams();
    if (limit && offset) {
      params = params.set('limit', limit);
      params = params.set('offset', offset);

    }
    // Map nos permite evaluar cada uno de los valores que vengan del observable
    // y podemos aplicar una transofrmacion
    return this.http.get<Product[]>(`${this.apiUrl}/products`, {params})
    .pipe(
      retry(3),
      map(products => products.map(item => {
        return {
          ...item,
          taxes: .19 * item.price
        };
      }))
    );
  }

  getProduct(id: string) {
    return this.http.get<Product>(`${this.apiUrl}/products/${id}`)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          if (error.status === 500) {
            return throwError('Ups algo esta fallando en el server');
          }
          if (error.status === 404) {
            return throwError('El producto no existe');
          }
          return throwError('Ups algo salio mal');
        })
      );
  }
 // Al usar Angular 10, los params deben de ser string
  getProductsByPage(limit: string, offset: string) {
    return this.http.get<Product[]>(`${this.apiUrl}`, {
      params: {limit, offset}
    });
  }

  create(data: CreateProductDTO) {
    return this.http.post<Product>(`${this.apiUrl}/products`, data);
  }

  update(id: string, data: UpdateProductDTO) {
    return this.http.put<Product>(`${this.apiUrl}/products/${id}`, data);
  }

  delete(id: string) {
    return this.http.delete<boolean>(`${this.apiUrl}/products/${id}`);
  }
}
